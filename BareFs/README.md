# BareFs [![NuGet](https://badgen.net/nuget/v/BareFs)](https://www.nuget.org/packages/BareFs)

F# Implementation of Binary Application Record Encoding (BARE) - https://baremessages.org/

Implements the specification from the [draft](https://datatracker.ietf.org/doc/draft-devault-bare/) version 1.

## Usage
### Disclaimer
This document will not cover BARE basics. Please refer to the [official documentation](https://baremessages.org/).

### Install package
With dotnet-CLI
```cmd
dotnet add package BareFs
```

With [paket](https://fsprojects.github.io/Paket/)
```cmd
paket add BareFs
```

> :bulb: The package provides support for [Fable](https://fable.io) to support BARE messages in JavaScript!

### Using a schema file
The prefered and simplest way is to use a schema file and let the code generator do the rest.

#### Install code generator
The code generator is available as dotnet tool and requires .NET 5!

```cmd
dotnet tool install bare-cli
```

#### Write the schema
Save the following content into `schema.bare`:
```
type PublicKey data<128>
type Time string # ISO 8601

enum Department {
    ACCOUNTING
    ADMINISTRATION
    CUSTOMER_SERVICE
    DEVELOPMENT

    # Reserved for the CEO
    JSMITH = 99
}

type Customer {
    name: string
    email: string
    address: Address
    orders: []{
        orderId: i64
        quantity: i32
    }
    metadata: map[string]data
}

type Employee {
    name: string
    email: string
    address: Address
    department: Department
    hireDate: Time
    publicKey: optional<PublicKey>
    metadata: map[string]data
}

type TerminatedEmployee void

type Person (Customer | Employee | TerminatedEmployee)

type Address {
    address: [4]string
    city: string
    state: string
    country: string
}
```

#### Generate the code
We take our schema file and let the generator save the code to `messages.fs`.

```cmd
dotnet bare schema.bare messages.fs --lang fs
```

#### Inject custom types into generated code
##### The problem
Some types like `System.Guid`, `decimal` or `System.DateTime` are not supported in BARE. In order to use those types the BARE schema needs a type which represents the unsupported type and a custom encoder and decoder handles the encoding.

If an unsupported type is used inside an BARE aggregate type, a message mapper would be needed to convert between BARE type and unsupported type. Let's take an example with an `Person` which has an id, a name and a birthday. We would like to implement the id with `System.Guid` and the birthday with `System.DateTime`. To use represent this in BARE we would end up with something like
```
type Guid data<16> # Byte representation of a System.Guid
type DateTime string # ISO 8601

type User {
    id: Guid
    name: string
    birthday: DateTime
}
```

The could generator would produce the types
```fsharp
type Guid = byte array

type DateTime = string

type User =
  {
    id: Guid
    name: string
    birthday: DateTime
  }
```

We can't create a new user with `{ id = System.Guid.Empty ; name = "Thomas" ; birthday = System.DateTime.Now }`. We would need to map the `System.Guid` to `byte array` and `System.DateTime` to `string`. And this would be needed for each creation of an user.

##### Custom types to the rescue
The code generator provides an option to declare custom types and the functions to encode/decode the value.

For this we keep our previous schema and store them as `schema.bare` and create public static methods to encode/decode the unsupported types. Let's assume in a project we would have the file `CustomEncoding.fs`
```fsharp
namespace BareExample
open System
open BareNET

module CustomEncoding
let ofGuid : Encoder<Guid> =
  fun (guid : Guid) -> guid.ToByteArray() |> Bare.encode_data_fixed_length 16

let decode_guid : Decoder<Guid> =
  Bare.decode_complex Guid
  |> Bare.apply (Bare.decode_data_fixed_length 16)

let ofDateTime : Encoder<DateTime> =
  fun (dateTime : DateTime) -> dateTime.ToString "u" |> Bare.encode_string |> Ok

let decode_DateTime : Decoder<DateTime> =
  Bare.decode_complex DateTime.Parse
  |> Bare.apply Bare.decode_string
```

We now use these encoders/decoders in the code generator by calling
```bash
dotnet bare schema.bare Messages.cs \
    --custom Guid System.Guid BareExample.CustomEncoding.ofGuid BareExample.CustomEncoding.decode_guid \
    -c DateTime System.DateTime BareExample.CustomEncoding.ofDateTime BareExample.CustomEncoding.decode_DateTime
```

> :bulb: Create a script which calls the generator, so you won't need to type all custom type declarations again

The code generator would produce
```fsharp
type Guid = System.Guid

type DateTime = System.DateTime

type User =
  {
    id: Guid
    name: string
    birthday: DateTime
  }

module Encoding =
  let ofGuid (value: Guid) : BareNET.Encoding_Result =
    value |> BareExample.CustomEncoding.ofGuid

  let decode_Guid : BareNET.Decoder<Guid> =
    BareExample.CustomEncoding.decode_guid

  let toGuid (data: byte array) : Result<Guid, string> =
    decode_Guid data |> Result.map fst

  let ofDateTime (value: DateTime) : BareNET.Encoding_Result =
    value |> BareExample.CustomEncoding.ofDateTime

  let decode_DateTime : BareNET.Decoder<DateTime> =
    BareExample.CustomEncoding.decode_DateTime

  let toDateTime (data: byte array) : Result<DateTime, string> =
    decode_DateTime data |> Result.map fst

  let ofUser (value: User) : BareNET.Encoding_Result =
    (ofGuid) value.id
    |> BareNET.Bare.andThen (BareNET.Bare.success (BareNET.Bare.encode_string)) value.name
    |> BareNET.Bare.andThen (ofDateTime) value.birthday

  let decode_User : BareNET.Decoder<User> =
    BareNET.Bare.decode_complex (fun (id: Guid) (name: string) (birthday: DateTime) -> { id = id ; name = name ; birthday = birthday })
    |> BareNET.Bare.apply (decode_Guid)
    |> BareNET.Bare.apply (BareNET.Bare.decode_string)
    |> BareNET.Bare.apply (decode_DateTime)

  let toUser (data: byte array) : Result<User, string> =
    decode_User data |> Result.map fst
```

That are the types we wanted! :smiley: We have an alias for our type an the underlying type will be encoded/decoded with our provided encoders/decoders.

The best thing about this, we can share the schema with other implementations without breaking it!

Let's recap what we gave the code generator as arguments.
- `--custom` or `-c` indicates we have a custom type.
- `Guid` and `DateTime` are the names of the types defined in the schema.
- `System.Guid` and `System.DateTime` are the types which replaces the schema types.
- `BareExample.CustomEncoding.ofGuid` and `BareExample.CustomEncoding.ofDateTime` are functions which encodes the unsupported value.
- `BareExample.CustomEncoding.decode_Guid` and `BareExample.CustomEncoding.decode_DateTime` are functions which decodes the unsupported value.

##### Using anonymous encoders/decoders
> :warning: This method introduces more point of failures. Use at own risk!

With the power of F# we can use custom types without the need to create custom encoders/decoders inside our project. Instead we use anonymous functions.
```bash
dotnet bare schema.bare Messages.cs \
    --custom Guid System.Guid \
        "(fun (guid: System.Guid) -> guid.ToByteArray() |> BareNET.Bare.encode_data_fixed_length 16)" \
        "(BareNET.Bare.decode_complex System.Guid |> BareNET.Bare.apply (BareNET.Bare.decode_data_fixed_length 16))" \
    -c DateTime System.DateTime \
        "(fun (dateTime: System.DateTime) -> dateTime.ToString \"u\" |> BareNET.Bare.encode_string |> Ok)" \
        "(BareNET.Bare.decode_complex System.DateTime.Parse |> BareNET.Bare.apply Bare.decode_string)"
```

##### Remarks!
- All custom types needs to be defined in the schema.
- The full name of the type needs to be provided. `Guid` would not be possible because the code generator would not know it's inside the `System` namespace.
- Encoders must be of type `BareNET.Encoder<T>`.
- Decoders must be of type `BareNET.Decoder<T>`.

### Writing your own encoder / decoder
BareFs delivers the building blocks to let you write your own encoder / decoder.

#### Encoders

| BARE type             | F# type       | method                                      |
|-----------------------|:-------------:|:--------------------------------------------|
| uint                  | `uint64`      | `Bare.encode_uint`                          |
| u8                    | `uint8`       | `Bare.encode_u8`                            |
| u16                   | `uint16`      | `Bare.encode_u16`                           |
| u32                   | `uint`        | `Bare.encode_u32`                           |
| u64                   | `uint64`      | `Bare.encode_u64`                           |
| int                   | `int64`       | `Bare.encode_int`                           |
| i8                    | `int8`        | `Bare.encode_i8`                            |
| i16                   | `int16`       | `Bare.encode_i16`                           |
| i32                   | `int`         | `Bare.encode_i32`                           |
| i64                   | `int64`       | `Bare.encode_i64`                           |
| f32                   | `float`       | `Bare.encode_f32`                           |
| f64                   | `double`      | `Bare.encode_f64`                           |
| bool                  | `bool`        | `Bare.encode_bool`                          |
| string                | `string`      | `Bare.encode_string`                        |
| enum                  | `enum<int32>` | `Bare.encode_enum`                          |
| data                  | `byte array`  | `Bare.encode_data`                          |
| data<length>          | `byte array`  | `Bare.encode_data_fixed_length`             |
| void                  | None          | Not encoded                                 |
| optional<type>        | `T option`    | `Bare.encode_optional`                      |
| [length]type          | `T seq`       | `Bare.encode_list_fixed_length`             |
| []type                | `T seq`       | `Bare.encode_list`                          |
| map[type A]type B     | `Map<A,B>`    | `Bare.encode_map`                           |
| (type \| type \| ...) | custom union  | `Bare.encode_union`                         |
| struct                | custom record | encoded values combined with `Bare.andThen` |

#### Decoders

| BARE type             | F# type       | method                                  |
|-----------------------|:-------------:|:----------------------------------------|
| uint                  | `uint64`      | `Bare.decode_uint`                      |
| u8                    | `uint8`       | `Bare.decode_u8`                        |
| u16                   | `uint16`      | `Bare.decode_u16`                       |
| u32                   | `uint`        | `Bare.decode_u32`                       |
| u64                   | `uint64`      | `Bare.decode_u64`                       |
| int                   | `int64`       | `Bare.decode_int`                       |
| i8                    | `int8`        | `Bare.decode_i8`                        |
| i16                   | `int16`       | `Bare.decode_i16`                       |
| i32                   | `int`         | `Bare.decode_i32`                       |
| i64                   | `int64`       | `Bare.decode_i64`                       |
| f32                   | `float`       | `Bare.decode_f32`                       |
| f64                   | `double`      | `Bare.decode_f64`                       |
| bool                  | `bool`        | `Bare.decode_bool`                      |
| string                | `string`      | `Bare.decode_string`                    |
| enum                  | `enum<int32>` | `Bare.decode_enum`                      |
| data                  | `byte array`  | `Bare.decode_data`                      |
| data<length>          | `byte array`  | `Bare.decode_data_fixed_length`         |
| void                  | None          | Not decoded                             |
| optional<type>        | `T option`    | `Bare.decode_optional`                  |
| [length]type          | `T seq`       | `Bare.decode_list_fixed_length`         |
| []type                | `T seq`       | `Bare.decode_list`                      |
| map[type A]type B     | `Map<A,B>`    | `Bare.decode_map`                       |
| (type \| type \| ...) | custom union  | `Bare.decode_union`                     |
| struct                | custom record | `Bare.decode_complex` with `Bare.apply` |

#### Unions
Encoders and Decoders for unions need a definition of the union to know what to encode/decode.

BareFs needs a case resolver for encoding and decoding which tells which case uses which identifier, encoder and decoder:
```fsharp
open BareNET

type MyUnion =
  | First
  | Second of int
  | Fourth of bool * string * byte array

let private myUnion_encoder_cases (value: MyUnion) =
  match value with
  | First -> Bare.void_case 1u
  | Second payload -> Bare.union_case 2u (Bare.encode_i32 payload)
  | Fourth (a, b, c) ->
      (Bare.success Bare.encode_bool) a
      |> Bare.andThen (Bare.success Bare.encode_string) b
      |> Bare.andThen (Bare.encode_data_fixed_length 2) c
      |> Bare.failable_case 4u

let myUnion_decoder_cases (identifier: uint) =
  match identifier with
  | 1u ->
      Bare.from_value First
  | 2u ->
      Bare.decode_complex Second
      |> Bare.apply Bare.decode_i32
  | 4u ->
      Bare.decode_complex (fun a b c -> Fourth (a, b, c))
      |> Bare.apply Bare.decode_bool
      |> Bare.apply Bare.decode_string
      |> Bare.apply (Bare.decode_data_fixed_length 2)
  | identifier ->
      Bare.error (sprintf "missing decoder for identifier %i" identifier)

[ First
  Second 13
  Second 9913
  Fourth (true, "Hello", [| 0uy ; 1uy |])
  First
  Fourth (false, "World", [| 13uy ; 34uy |])
] |> List.iter (fun value ->
  value
  |> Bare.encode_union myUnion_encoder_cases
  |> Result.bind (Bare.decode_union myUnion_decoder_cases) // Result<(MyUnion, byte array), string>
  |> Result.map fst // Result<MyUnion, string>
)
```

#### Structs
Structs, known as (anonymous) records in F#, are just the values concatenated. Let's take an example with an address struct.

```fsharp
type Address =
  { Address : string array
    City : string
    State : string
    Country : string
  }
```

BareFs provides a simple to use API to combine encoding results.

```fsharp
open BareNET

let ofAddress (address : Address) : Encoding_Result =
  Bare.encode_list_fixed_length 4 address.Address // Let's assume that a factory function ensures the address always contains 4 parts
  |> Bare.andThen (Bare.success Bare.encode_string) address.City // Bare.success tells Bare.andThen that this encoder always succeeds
  |> Bare.andThen (Bare.success Bare.encode_string) address.State
  |> Bare.andThen (Bare.success Bare.encode_string) address.Country
```

Great! We have our address encoder. Let's go to the decoder!


```fsharp
open BareNET

let decode_Address : Decoder<Address> =
  Bare.decode_complex (fun address city state country -> { Address = address ; City = city ; State = state ; Country = country })
  |> Bare.apply (Bare.decode_list_fixed_length 4 Bare.decode_string) // Address decoding
  |> Bare.apply Bare.decode_string // City decoding
  |> Bare.apply Bare.decode_string // State decoding
  |> Bare.apply Bare.decode_string // Country decoding
```


Yeah! What a beautiful decoder. Let's wrap up what we did there.

By defining a `Decoder<Address>` we said `decode_Address` can take some `byte array` and gives back a `Decoding_Result<Address>` which may contain an Address and remaining bytes if the given data represents at least an Address.

With `Bare.decode_complex` we declared a function whichs creates our wanted value. The function can have as many arguments as needed for the wanted type.

Then we used `Bare.apply` to provide the decoder for the next argument, which needs to decode an `string array`. So we provided the fixed length list decoder.
This step is repeated until decoders for all arguments are provided.

If we want to encode an address we simple give the data to our encoder.

```fsharp
decode_Address [| 0x2Fuy ; 0x01uy ; ... |]
```

## How do I encode types like Guid or DateTime?
Great question! BARE supports a few primitive and aggregate types and won't support more. The reason behind this is to be closed and simple but extensible, so that you can share your schema between other implementations.

To archive the encoding and decoding of other types which are language specific, you have to convert the specific type into a BARE type. Let's take a look at the `Guid` type.
We can convert a `Guid` to `string` or `byte[16]` supported by .NET itself. So let's take advantage of that and write our own type (shown in BARE schema):

```
type UUID data<16>
```

Because I want my message as small as possible I went with the `byte[16]` type, which equals to the BARE type `data<16>`. My encoder would take the `Guid`, converts it to `byte array` and encodes it with `encode_data_fixed_length`. The decoder would do the opposite.

```fsharp
open System
open BareNET

let ofGuid : Encoder<Guid> =
  fun (guid : Guid) -> guid.ToByteArray() |> Bare.encode_data_fixed_length 16

let decode_guid : Decoder<Guid> =
  Bare.define_complex Guid
  |> Bare.apply (Bare.decode_data_fixed_length 16)

let toGuid (data: byte array) : Result<Guid, string> =
  decode_guid data |> Result.map fst
```

Or we make use of the code generator and write a mapper to convert the `Guid`.

```fsharp
open System
open Bare.Msg // Default namespace of the generated code

let encode_guid : Encoder<Guid> =
  fun (guid : Guid) -> guid.ToByteArray() |> Encoding.UUID_encoded

let decode_guid : Decoder<Guid> =
  Bare.define_complex Guid
  |> Bare.apply (Encoding.decode_UUID)
```

## Limitations
- Reflection is not supported, you have to write your own encoder / decoder or use the code generator
- Enums must use `int32` as the underlying type
- Length of data and list types is limited to `Int32.MaxValue` (2.147.483.647)