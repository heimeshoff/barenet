﻿namespace BareNET.Schema
{
    public interface UserType { }
    public interface Type { }
    public interface NonEnumType : Type { }
    public interface AggregateType : NonEnumType { }

    public enum TypeKind
    {
        Int,
        I8,
        I16,
        I32,
        I64,
        UInt,
        U8,
        U16,
        U32,
        U64,
        F32,
        F64,
        Bool,
        String,
        Data,
        Void
    }

    public readonly struct NamedUserType : UserType
    {
        public readonly string Name;
        public readonly NonEnumType Type;

        public NamedUserType(string name, NonEnumType type)
        {
            Name = name;
            Type = type;
        }
    }

    public readonly struct EnumUserType : UserType
    {
        public readonly string Name;
        public readonly EnumType Type;

        public EnumUserType(string name, EnumType type)
        {
            Name = name;
            Type = type;
        }
    }

    public readonly struct PrimitiveType : NonEnumType
    {
        public readonly TypeKind Kind;
        public readonly ulong? Length; // In case of data with fixed length

        public PrimitiveType(TypeKind kind, ulong? length)
        {
            Kind = kind;
            Length = length;
        }
    }

    public readonly struct UserTypeName : NonEnumType
    {
        public readonly string Name;

        public UserTypeName(string name)
        {
            Name = name;
        }
    }

    public readonly struct EnumType : Type
    {
        public readonly EnumValue[] Values;

        public EnumType(EnumValue[] values)
        {
            Values = values;
        }

        public bool Equals(EnumType other)
        {
            if (Values.Length != other.Values.Length) return false;
            for (var i = 0; i < Values.Length; i++)
            {
                if (!Values[i].Equals(other.Values[i])) return false;
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            return obj is EnumType other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Values.GetHashCode();
        }
    }

    public readonly struct EnumValue
    {
        public readonly string Name;
        public readonly uint? Value;

        public EnumValue(string name, uint? value)
        {
            Name = name;
            Value = value;
        }
    }

    public readonly struct OptionalType : AggregateType
    {
        public readonly NonEnumType Type;

        public OptionalType(NonEnumType type)
        {
            Type = type;
        }
    }

    public readonly struct ListType : AggregateType
    {
        public readonly NonEnumType Type;
        public readonly ulong? Length;

        public ListType(NonEnumType type, ulong? length)
        {
            Type = type;
            Length = length;
        }
    }

    public readonly struct MapType : AggregateType
    {
        public readonly NonEnumType KeyType;
        public readonly NonEnumType ValueType;

        public MapType(NonEnumType keyType, NonEnumType valueType)
        {
            KeyType = keyType;
            ValueType = valueType;
        }
    }

    public readonly struct UnionType : AggregateType
    {
        public readonly UnionMember[] Members;

        public UnionType(UnionMember[] members)
        {
            Members = members;
        }

        public bool Equals(UnionType other)
        {
            if (Members.Length != other.Members.Length) return false;
            for (var i = 0; i < Members.Length; i++)
            {
                if (!Members[i].Equals(other.Members[i])) return false;
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            return obj is UnionType other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Members.GetHashCode();
        }
    }

    public readonly struct UnionMember
    {
        public readonly NonEnumType Type;
        public readonly int? Identifier;

        public UnionMember(NonEnumType type, int? identifier)
        {
            Type = type;
            Identifier = identifier;
        }
    }

    public readonly struct StructType : AggregateType
    {
        public readonly StructField[] Fields;

        public StructType(StructField[] fields)
        {
            Fields = fields;
        }

        public bool Equals(StructType other)
        {
            if (Fields.Length != other.Fields.Length) return false;
            for (var i = 0; i < Fields.Length; i++)
            {
                if (!Fields[i].Equals(other.Fields[i])) return false;
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            return obj is StructType other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Fields.GetHashCode();
        }
    }

    public readonly struct StructField
    {
        public readonly string Name;
        public readonly NonEnumType Type;

        public StructField(string name, NonEnumType type)
        {
            Name = name;
            Type = type;
        }
    }
}