﻿module BareFs.Specs.Primitives

open System
open NUnit.Framework
open BareNET

type TestEnum =
  | First = 2
  | Second = 3
  | Third = 5

type FailingEnum =
  | NegativeValue = -20

let has_remaining_bytes (bytes: byte array) (result : Decoding_Result<_>) =
  match result with
  | Ok (_, rest) -> Assert.AreEqual (bytes, rest)
  | Error error -> Assert.Fail error

let roundtrip (encoder : 'T -> byte array) (decoder : byte array -> Decoding_Result<'T>) value =
  match encoder value |> decoder with
  | Ok (decoded, _) -> Assert.AreEqual (value, decoded)
  | Error error -> Assert.Fail error

let failable_roundtrip (encoder : 'T -> Encoding_Result) (decoder : byte array -> Decoding_Result<'T>) value =
  match encoder value |> Result.bind decoder with
  | Ok (decoded, _) -> Assert.AreEqual (value, decoded)
  | Error error -> Assert.Fail error

let expect_to_fail_with error (result : Result<_, string>) =
  match result with
  | Ok _ -> Assert.Fail "Expected error but succeeded"
  | Error err -> Assert.AreEqual (error, err)

let expect_to_succeed_with (expected: 'T) (result : Result<'T, string>) =
  match result with
  | Ok value -> Assert.AreEqual (expected, value)
  | Error err -> Assert.Fail err

let expect_to_equals (expected: 'T) (actual: 'T) =
  Assert.AreEqual (expected, actual)

[<Test>]
let Type_u8_takes_only_needed_bytes () =
  Bare.decode_u8 [| 0x01uy; 0x02uy; 0x03uy |]
  |> has_remaining_bytes [| 0x02uy ; 0x03uy |]

[<Test>]
let Type_u8 () =
  [Byte.MinValue..Byte.MaxValue]
  |> List.iter (roundtrip Bare.encode_u8 Bare.decode_u8)

[<Test>]
let Type_i8_takes_only_needed_bytes () =
  Bare.decode_i8 [| 0x01uy ; 0x02uy ; 0x03uy |]
  |> has_remaining_bytes [| 0x02uy ; 0x03uy |]

[<Test>]
let Type_i8_uses_two_s_complement () =
  Bare.encode_i8 -1y
  |> expect_to_equals [| 0xFFuy |]

[<Test>]
let Type_i8 () =
  [SByte.MinValue..SByte.MaxValue]
  |> List.iter (roundtrip Bare.encode_i8 Bare.decode_i8)

[<Test>]
let Type_u16_takes_only_needed_bytes () =
  Bare.decode_u16 [| 0x01uy ; 0x02uy ; 0x03uy |]
  |> has_remaining_bytes [| 0x03uy |]

[<Test>]
let Type_u16_is_encoded_in_little_endian () =
  Bare.encode_u16 0x0102us
  |> expect_to_equals [| 0x02uy ; 0x01uy |]

[<Test>]
let Type_u16 () =
  [UInt16.MinValue..UInt16.MaxValue]
  |> List.iter (roundtrip Bare.encode_u16 Bare.decode_u16)

[<Test>]
let Type_i16_takes_only_needed_bytes () =
  Bare.decode_i16 [| 0x01uy ; 0x02uy ; 0x03uy |]
  |> has_remaining_bytes [| 0x03uy |]

[<Test>]
let Type_i16_uses_two_s_complement () =
  Bare.encode_i16 -1s
  |> expect_to_equals [| 0xFFuy ; 0xFFuy |]

[<Test>]
let Type_i16_is_encoded_in_little_endian () =
  Bare.encode_i16 0x0102s
  |> expect_to_equals [| 0x02uy ; 0x01uy |]

[<Test>]
let Type_i16 () =
  [Int16.MinValue..Int16.MaxValue]
  |> List.iter (roundtrip Bare.encode_i16 Bare.decode_i16)

[<Test>]
let Type_u32_takes_only_needed_bytes () =
  Bare.decode_u32 [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy ; 0x05uy ; 0x06uy |]
  |> has_remaining_bytes [| 0x05uy ; 0x06uy |]

[<Test>]
let Type_u32_is_encoded_in_little_endian () =
  Bare.encode_u32 0x01020304u
  |> expect_to_equals [| 0x04uy ; 0x03uy ; 0x02uy ; 0x01uy |]

[<Test>]
let Type_u32 () =
  let rec iter value action =
    if value = 0u then ()
    else value |> action ; iter (value>>>2) action
  iter UInt32.MaxValue (roundtrip Bare.encode_u32 Bare.decode_u32)

[<Test>]
let Type_i32_takes_only_needed_bytes () =
  Bare.decode_i32 [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy ; 0x05uy ; 0x06uy |]
  |> has_remaining_bytes [| 0x05uy ; 0x06uy |]

[<Test>]
let Type_i32_uses_two_s_complement () =
  Bare.encode_i32 -1
  |> expect_to_equals [| 0xFFuy ; 0xFFuy ; 0xFFuy ; 0xFFuy |]

[<Test>]
let Type_i32_is_encoded_in_little_endian () =
  Bare.encode_i32 0x01020304
  |> expect_to_equals [| 0x04uy ; 0x03uy ; 0x02uy ; 0x01uy |]

[<Test>]
let Type_i32 () =
  let rec iter endValue value action =
    if value = endValue then ()
    else value |> action ; iter endValue (value>>>1) action
  iter -1 Int32.MinValue (roundtrip Bare.encode_i32 Bare.decode_i32)
  iter 0 Int32.MaxValue (roundtrip Bare.encode_i32 Bare.decode_i32)

[<Test>]
let Type_u64_takes_only_needed_bytes () =
  Bare.decode_u64 [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy ; 0x05uy ; 0x06uy ; 0x07uy ; 0x08uy ; 0x09uy ; 0x10uy |]
  |> has_remaining_bytes [| 0x09uy ; 0x10uy |]

[<Test>]
let Type_u64_is_encoded_in_little_endian () =
  Bare.encode_u64 0x0102030405060708UL
  |> expect_to_equals [| 0x08uy ; 0x07uy ; 0x06uy ; 0x05uy ; 0x04uy ; 0x03uy ; 0x02uy ; 0x01uy |]

[<Test>]
let Type_u64 () =
  let rec iter value action =
    if value = 0UL then ()
    else value |> action ; iter (value>>>2) action
  iter UInt64.MaxValue (roundtrip Bare.encode_u64 Bare.decode_u64)

[<Test>]
let Type_i64_takes_only_needed_bytes () =
  Bare.decode_i64 [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy ; 0x05uy ; 0x06uy ; 0x07uy ; 0x08uy ; 0x09uy ; 0x10uy |]
  |> has_remaining_bytes [| 0x09uy ; 0x10uy |]

[<Test>]
let Type_i64_uses_two_s_complement () =
  Bare.encode_i64 -1L
  |> expect_to_equals [| 0xFFuy ; 0xFFuy ; 0xFFuy ; 0xFFuy ; 0xFFuy ; 0xFFuy ; 0xFFuy ; 0xFFuy |]

[<Test>]
let Type_i64_is_encoded_in_little_endian () =
  Bare.encode_i64 0x0102030405060708L
  |> expect_to_equals [| 0x08uy ; 0x07uy ; 0x06uy ; 0x05uy ; 0x04uy ; 0x03uy ; 0x02uy ; 0x01uy |]

[<Test>]
let Type_i64 () =
  let rec iter endValue value action =
    if value = endValue then ()
    else value |> action ; iter endValue (value>>>1) action
  iter -1L Int64.MinValue (roundtrip Bare.encode_i64 Bare.decode_i64)
  iter 0L Int64.MaxValue (roundtrip Bare.encode_i64 Bare.decode_i64)

[<Test>]
let Type_uint_encodes_in_variable_length__value_is_splitted_in_7_bit_groups__most_significant_bit_is_not_set_for_last_octet () =
  Bare.encode_uint 127UL
  |> expect_to_equals [| 0b0111_1111uy |]
  Bare.encode_uint 128UL
  |> expect_to_equals [| 0b1000_0000uy ; 0b0000_0001uy |]
  Bare.encode_uint 16384UL
  |> expect_to_equals [| 0b1000_0000uy ; 0b1000_0000uy ; 0b0000_0001uy |]
  Bare.encode_uint 72057594037927935UL
  |> expect_to_equals [| 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b0111_1111uy |]
  Bare.encode_uint UInt64.MaxValue
  |> expect_to_equals [| 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b0000_0001uy |]

[<Test>]
let Type_uint_is_encoded_in_little_endian () =
  Bare.encode_uint 0b1110011_0011100_0001111UL
  |> expect_to_equals [| 0b10001111uy ; 0b10011100uy ; 0b01110011uy |]

[<Test>]
let Type_uint_takes_only_needed_bytes () =
  Bare.decode_uint [| 0b10001111uy ; 0b01111000uy ; 0x02uy ; 0x3Euy |]
  |> has_remaining_bytes [| 0x02uy ; 0x3Euy |]

[<Test>]
let Type_uint () =
  let rec iter value action =
    if value = 0UL then ()
    else value |> action ; iter (value>>>1) action
  iter UInt64.MaxValue (roundtrip Bare.encode_uint Bare.decode_uint)

[<Test>]
let Type_int_encodes_in_variable_length__value_is_splitted_in_7_bit_groups__most_significant_bit_is_not_set_for_last_octet () =
  Bare.encode_int 63L
  |> expect_to_equals [| 0b0111_1110uy |]
  Bare.encode_int 64L
  |> expect_to_equals [| 0b1000_0000uy ; 0b0000_0001uy |]
  Bare.encode_int 8192L
  |> expect_to_equals [| 0b1000_0000uy ; 0b1000_0000uy ; 0b0000_0001uy |]
  Bare.encode_int Int64.MaxValue
  |> expect_to_equals [| 0b1111_1110uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b1111_1111uy ; 0b0000_0001uy |]

[<Test>]
let Type_int_is_encoded_in_zig_zag () =
  Bare.encode_int -2L |> expect_to_equals [| 0b00000011uy |]
  Bare.encode_int -1L |> expect_to_equals [| 0b00000001uy |]
  Bare.encode_int 0L |> expect_to_equals [| 0b00000000uy |]
  Bare.encode_int 1L |> expect_to_equals [| 0b00000010uy |]
  Bare.encode_int 2L |> expect_to_equals [| 0b00000100uy |]

[<Test>]
let Type_int_is_encoded_in_little_endian () =
  Bare.encode_int 0b1110011_0011100_0001111L
  |> expect_to_equals [| 0b10011110uy ; 0b10111000uy ; 0b11100110uy ; 0b00000001uy |]

[<Test>]
let Type_int_takes_only_needed_bytes () =
  Bare.decode_int [| 0b10001111uy ; 0b01111000uy ; 0x02uy ; 0x3Euy |]
  |> has_remaining_bytes [| 0x02uy ; 0x3Euy |]

[<Test>]
let Type_int () =
  let rec iter endValue value action =
    if value = endValue then ()
    else value |> action ; iter endValue (value>>>1) action
  iter -1L Int64.MinValue (roundtrip Bare.encode_int Bare.decode_int)
  iter 0L Int64.MaxValue (roundtrip Bare.encode_int Bare.decode_int)

[<Test>]
let Type_f32_returns_error_if_value_is_NaN () =
  Bare.encode_f32 Single.NaN
  |> expect_to_fail_with "value is not a number"

[<Test>]
let Type_f32_takes_only_needed_bytes () =
  Bare.decode_f32 [| 0x00uy ; 0x1Euy ; 0x22uy ; 0x3Fuy ; 0x8Auy ; 0xE4uy |]
  |> has_remaining_bytes [| 0x8Auy ; 0xE4uy |]

[<Test>]
let Type_f32 () =
  let r = Random (int DateTime.Now.Ticks &&& 0x0000FFFF);
  List.init 20 (fun _ -> r.NextDouble() |> float32)
  |> List.iter (failable_roundtrip Bare.encode_f32 Bare.decode_f32)

[<Test>]
let Type_f64_returns_error_if_value_is_NaN () =
  Bare.encode_f64 Double.NaN
  |> expect_to_fail_with "value is not a number"

[<Test>]
let Type_f64_takes_only_needed_bytes () =
  Bare.decode_f64 [| 0x00uy ; 0x1Euy ; 0x22uy ; 0x3Fuy ; 0x00uy ; 0x1Euy ; 0x22uy ; 0x3Fuy ; 0x8Auy ; 0xE4uy |]
  |> has_remaining_bytes [| 0x8Auy ; 0xE4uy |]

[<Test>]
let Type_f64 () =
  let r = Random (int DateTime.Now.Ticks &&& 0x0000FFFF);
  List.init 20 (fun _ -> r.NextDouble())
  |> List.iter (failable_roundtrip Bare.encode_f64 Bare.decode_f64)

[<Test>]
let Type_bool_greater_one_throws_exception () =
  Bare.decode_bool [| 0x02uy |]
  |> expect_to_fail_with "data does not represent a boolean"

[<Test>]
let Type_bool () =
  roundtrip Bare.encode_bool Bare.decode_bool true
  roundtrip Bare.encode_bool Bare.decode_bool false

[<Test>]
let Type_enum_returns_error_if_value_is_not_a_member_of_enum () =
  Bare.decode_enum<TestEnum> [| 0x01uy |]
  |> expect_to_fail_with "value is not represented in enum"

[<Test>]
let Type_enum_decoding_negative_value_throws_exception () =
  Bare.encode_enum<FailingEnum> FailingEnum.NegativeValue
  |> expect_to_fail_with "enum value can't be negative"

[<Test>]
let Type_enum () =
  [ TestEnum.First ; TestEnum.Second ; TestEnum.Third ]
  |> List.iter (failable_roundtrip Bare.encode_enum<TestEnum> Bare.decode_enum<TestEnum>)

[<Test>]
let Type_string_encodes_length_in_octets_as_uint_in_first_place () =
  Bare.encode_string "Hello World"
  |> expect_to_equals [| 0x0Buy ; 0x48uy ; 0x65uy ; 0x6Cuy ; 0x6Cuy ; 0x6Fuy ; 0x20uy ; 0x57uy ; 0x6Fuy ; 0x72uy ; 0x6Cuy ; 0x64uy |]

[<Test>]
let Type_string_takes_only_needed_bytes () =
  Bare.decode_string [| 0x0Buy ; 0x48uy ; 0x65uy ; 0x6Cuy ; 0x6Cuy ; 0x6Fuy ; 0x20uy ; 0x57uy ; 0x6Fuy ; 0x72uy ; 0x6Cuy ; 0x64uy ; 0x02uy ; 0xF2uy |]
  |> has_remaining_bytes [| 0x02uy ; 0xF2uy |]

[<Test>]
let Type_string () =
  [ "Hello World!"
    "∮ E⋅da = Q,  n → ∞, ∑ f(i) = ∏ g(i), ∀x∈ℝ: ⌈x⌉ = −⌊−x⌋, α ∧ ¬β = ¬(¬α ∨ β)"
    @"Οὐχὶ ταὐτὰ παρίσταταί μοι γιγνώσκειν, ὦ ἄνδρες ᾿Αθηναῖοι,
    ὅταν τ᾿ εἰς τὰ πράγματα ἀποβλέψω καὶ ὅταν πρὸς τοὺς
    λόγους οὓς ἀκούω· τοὺς μὲν γὰρ λόγους περὶ τοῦ
    τιμωρήσασθαι Φίλιππον ὁρῶ γιγνομένους, τὰ δὲ πράγματ᾿
    εἰς τοῦτο προήκοντα,  ὥσθ᾿ ὅπως μὴ πεισόμεθ᾿ αὐτοὶ
    πρότερον κακῶς σκέψασθαι δέον. οὐδέν οὖν ἄλλο μοι δοκοῦσιν
    οἱ τὰ τοιαῦτα λέγοντες ἢ τὴν ὑπόθεσιν, περὶ ἧς βουλεύεσθαι,
    οὐχὶ τὴν οὖσαν παριστάντες ὑμῖν ἁμαρτάνειν. ἐγὼ δέ, ὅτι μέν
    ποτ᾿ ἐξῆν τῇ πόλει καὶ τὰ αὑτῆς ἔχειν ἀσφαλῶς καὶ Φίλιππον
    τιμωρήσασθαι, καὶ μάλ᾿ ἀκριβῶς οἶδα· ἐπ᾿ ἐμοῦ γάρ, οὐ πάλαι
    γέγονεν ταῦτ᾿ ἀμφότερα· νῦν μέντοι πέπεισμαι τοῦθ᾿ ἱκανὸν
    προλαβεῖν ἡμῖν εἶναι τὴν πρώτην, ὅπως τοὺς συμμάχους
    σώσομεν. ἐὰν γὰρ τοῦτο βεβαίως ὑπάρξῃ, τότε καὶ περὶ τοῦ
    τίνα τιμωρήσεταί τις καὶ ὃν τρόπον ἐξέσται σκοπεῖν· πρὶν δὲ
    τὴν ἀρχὴν ὀρθῶς ὑποθέσθαι, μάταιον ἡγοῦμαι περὶ τῆς
    τελευτῆς ὁντινοῦν ποιεῖσθαι λόγον."
  ]
  |> List.iter (roundtrip Bare.encode_string Bare.decode_string)

[<Test>]
let Type_data_fixed_length_encodes_data_literally_in_the_message () =
  Bare.encode_data_fixed_length 4 [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy |]
  |> expect_to_succeed_with [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy |]

[<Test>]
let Type_data_fixed_length_takes_only_needed_bytes () =
  Bare.decode_data_fixed_length 4 [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy ; 0xF2uy ; 0xACuy |]
  |> has_remaining_bytes [| 0xF2uy ; 0xACuy |]

[<Test>]
let Type_data_fixed_length_returns_error_if_data_is_less_than_length () =
  Bare.encode_data_fixed_length 4 [| 0x01uy ; 0x02uy |]
  |> expect_to_fail_with "data is not the needed length"

[<Test>]
let Type_data_fixed_length_returns_error_if_length_is_zero () =
  Bare.encode_data_fixed_length 0 [||]
  |> expect_to_fail_with "length must be at least 1"

[<Test>]
let Type_data_fixed_length () =
  [ [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy |]
    [| 0x05uy ; 0x06uy ; 0x07uy ; 0x08uy |]
    [| 0x09uy ; 0x0Auy ; 0x0Buy ; 0x0Cuy |]
    [| 0x0Duy ; 0x0Euy ; 0x0Fuy ; 0x10uy |]
  ]
  |> List.iter (failable_roundtrip (Bare.encode_data_fixed_length 4) (Bare.decode_data_fixed_length 4))

[<Test>]
let Type_data_encodes_length_at_first_place_followed_by_the_data () =
  Bare.encode_data [| 0x02uy ; 0x05uy ; 0xA2uy |]
  |> expect_to_equals [| 0x03uy ; 0x02uy ; 0x05uy ; 0xA2uy |]

[<Test>]
let Type_data_takes_only_needed_bytes () =
  Bare.decode_data [| 0x04uy ; 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy ; 0xF2uy ; 0xACuy |]
  |> has_remaining_bytes [| 0xF2uy ; 0xACuy |]

[<Test>]
let Type_data_empty_data () =
  Bare.encode_data [||] |> expect_to_equals [| 0x00uy |]

[<Test>]
let Type_data () =
  [ [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy |]
    [| 0x05uy ; 0x06uy |]
    [| 0x09uy ; 0x0Auy ; 0x0Buy ; 0x0Cuy ; 0xFEuy ; 0xAAuy ; 0xEEuy ; 0x2Fuy |]
    [| 0x0Duy |]
  ]
  |> List.iter (roundtrip Bare.encode_data Bare.decode_data)