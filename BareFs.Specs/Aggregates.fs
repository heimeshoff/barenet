module BareFs.Specs.Aggregates

open System
open NUnit.Framework
open BareNET
open Primitives

let expect_decoded (value: 'T) (result: Decoding_Result<'T>) =
  result
  |> Result.map fst
  |> expect_to_succeed_with value

[<Test>]
let Type_optional_encodes_no_value_as_boolean_false () =
  Bare.encode_optional (Bare.success Bare.encode_i8) None
  |> expect_to_succeed_with [| 0x00uy |]

[<Test>]
let Type_optional_encodes_value_as_boolean_true_followed_by_encoded_value () =
  Some 27y
  |> Bare.encode_optional (Bare.success Bare.encode_i8)
  |> expect_to_succeed_with [| 0x01uy ; 0x1Buy |]

[<Test>]
let Type_optional_only_takes_needed_bytes () =
  Bare.decode_optional Bare.decode_i8 [| 0x00uy ; 0x13uy ; 0xA6uy ; 0xE0uy |]
  |> has_remaining_bytes [| 0x13uy ; 0xA6uy ; 0xE0uy |]
  Bare.decode_optional Bare.decode_i8 [| 0x01uy ; 0x27uy ; 0x13uy ; 0xA6uy ; 0xE0uy |]
  |> has_remaining_bytes [| 0x13uy ; 0xA6uy ; 0xE0uy |]

[<Test>]
let Type_optional () =
  Some 20 |> failable_roundtrip (Bare.encode_optional (Bare.success Bare.encode_i32)) (Bare.decode_optional Bare.decode_i32)
  None |> failable_roundtrip (Bare.encode_optional (Bare.success Bare.encode_i32)) (Bare.decode_optional Bare.decode_i32)
  Some 9313u |> failable_roundtrip (Bare.encode_optional (Bare.success Bare.encode_u32)) (Bare.decode_optional Bare.decode_u32)
  None |> failable_roundtrip (Bare.encode_optional (Bare.success Bare.encode_u32)) (Bare.decode_optional Bare.decode_u32)
  Some true |> failable_roundtrip (Bare.encode_optional (Bare.success Bare.encode_bool)) (Bare.decode_optional Bare.decode_bool)
  None |> failable_roundtrip (Bare.encode_optional (Bare.success Bare.encode_bool)) (Bare.decode_optional Bare.decode_bool)
  Some "Hello World" |> failable_roundtrip (Bare.encode_optional (Bare.success Bare.encode_string)) (Bare.decode_optional Bare.decode_string)
  None |> failable_roundtrip (Bare.encode_optional (Bare.success Bare.encode_string)) (Bare.decode_optional Bare.decode_string)
  Some [| 0x04uy ; 0x3Duy ; 0x20uy |] |> failable_roundtrip (Bare.encode_optional (Bare.encode_data_fixed_length 3)) (Bare.decode_optional (Bare.decode_data_fixed_length 3))
  None |> failable_roundtrip (Bare.encode_optional (Bare.encode_data_fixed_length 3)) (Bare.decode_optional (Bare.decode_data_fixed_length 3))

[<Test>]
let Type_list_fixed_length_encodes_data_literally_in_the_message () =
  [ 0x01y ; 0x02y ; 0x03y ; 0x04y ]
  |> Bare.encode_list_fixed_length 4 (Bare.success Bare.encode_i8)
  |> expect_to_succeed_with [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy |]

[<Test>]
let Type_list_fixed_length_takes_only_needed_bytes () =
  [| 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy ; 0xF2uy ; 0xACuy |]
  |> Bare.decode_list_fixed_length 4 Bare.decode_i8
  |> has_remaining_bytes [| 0xF2uy ; 0xACuy |]

[<Test>]
let Type_list_fixed_length_returns_first_encoder_error () =
  [ [| 0x00uy |] ; [| 0x02uy ; 0x02uy |] ]
  |> Bare.encode_list_fixed_length 2 (Bare.encode_data_fixed_length 1)
  |> expect_to_fail_with "data is not the needed length"

[<Test>]
let Type_list_fixed_length_returns_error_if_data_is_less_than_length () =
  [ 0x02y ; 0x03y ]
  |> Bare.encode_list_fixed_length 5 (Bare.success Bare.encode_i8)
  |> expect_to_fail_with "data is not the needed length"

[<Test>]
let Type_list_fixed_length_returns_error_if_length_is_zero () =
  Bare.encode_list_fixed_length 0 (Bare.success Bare.encode_i8) []
  |> expect_to_fail_with "length must be at least 1"

[<Test>]
let Type_list_fixed_length () =
  seq [|  4 ; 19 ; 10 ; 2246 ; 93 |]
  |> failable_roundtrip (Bare.encode_list_fixed_length 5 (Bare.success Bare.encode_i32)) (Bare.decode_list_fixed_length 5 Bare.decode_i32)
  seq [ "Hello" ; "World" ]
  |> failable_roundtrip (Bare.encode_list_fixed_length 2 (Bare.success Bare.encode_string)) (Bare.decode_list_fixed_length 2 Bare.decode_string)
  seq [ None ; Some 13.31f ; Some 9.3157f ; None ; None ; Some 3.113f ]
  |> failable_roundtrip (Bare.encode_list_fixed_length 6 (Bare.encode_optional Bare.encode_f32)) (Bare.decode_list_fixed_length 6 (Bare.decode_optional Bare.decode_f32))

[<Test>]
let Type_list_encodes_length_of_values_as_uint_in_first_place_followed_by_the_encoded_values () =
  Bare.encode_list (Bare.success Bare.encode_i16) [ 4s ; 305s ]
  |> expect_to_succeed_with [| 0x02uy ; 0x04uy ; 0x00uy ; 0x31uy ; 0x01uy |]

[<Test>]
let Type_list_only_takes_needed_bytes () =
  [| 0x04uy ; 0x01uy ; 0x02uy ; 0x03uy ; 0x04uy ; 0xF2uy ; 0xACuy |]
  |> Bare.decode_list Bare.decode_u8
  |> has_remaining_bytes [| 0xF2uy ; 0xACuy |]

[<Test>]
let Type_list_zero_length () =
  Bare.encode_list (Bare.success Bare.encode_u8) []
  |> expect_to_succeed_with [| 0x00uy |]

[<Test>]
let Type_list_returns_first_encoder_error () =
  [ [| 0x00uy |] ; [| 0x02uy ; 0x02uy |] ]
  |> Bare.encode_list (Bare.encode_data_fixed_length 1)
  |> expect_to_fail_with "data is not the needed length"

[<Test>]
let Type_list () =
  seq [ 4 ; 19 ; 10 ; 2246 ; 93 ]
  |> failable_roundtrip (Bare.encode_list (Bare.success Bare.encode_i32)) (Bare.decode_list Bare.decode_i32)
  seq [ "Hello" ; "World" ]
  |> failable_roundtrip (Bare.encode_list (Bare.success Bare.encode_string)) (Bare.decode_list Bare.decode_string)
  seq [ None ; Some 13.31f ; Some 9.3157f ; None ; None ; Some 3.113f ]
  |> failable_roundtrip (Bare.encode_list (Bare.encode_optional Bare.encode_f32)) (Bare.decode_list (Bare.decode_optional Bare.decode_f32))

[<Test>]
let Type_map_encodes_length_of_values_as_uint_in_first_place_followed_by_encoded_key_value_pairs_concatenated () =
  Map.ofList [ (13s, "Hello World") ]
  |> Bare.encode_map (Bare.success Bare.encode_i16) (Bare.success Bare.encode_string)
  |> expect_to_succeed_with [| 0x01uy ; 0x0Duy ; 0x00uy ; 0x0Buy ; 0x48uy ; 0x65uy ; 0x6Cuy ; 0x6Cuy ; 0x6Fuy ; 0x20uy ; 0x57uy ; 0x6Fuy ; 0x72uy ; 0x6Cuy ; 0x64uy |]

[<Test>]
let Type_map_zero_length () =
  Map.empty
  |> Bare.encode_map (Bare.success Bare.encode_i16) (Bare.success Bare.encode_string)
  |> expect_to_succeed_with [| 0x00uy |]

[<Test>]
let Type_map_only_takes_needed_bytes () =
  [| 0x02uy ; 0x01uy ; 0x00uy ; 0x02uy ; 0x00uy ; 0xFEuy ; 0x2Auy |]
  |> Bare.decode_map Bare.decode_u8 Bare.decode_i8
  |> has_remaining_bytes [| 0xFEuy ; 0x2Auy |]

[<Test>]
let Type_map_returns_error_if_key_is_duplicated () =
  [| 0x02uy ; 0x01uy ; 0x00uy ; 0x01uy ; 0x00uy |]
  |> Bare.decode_map Bare.decode_u8 Bare.decode_i8
  |> expect_to_fail_with "key is duplicated"

[<Test>]
let Type_map_returns_first_encoder_error () =
  Map.ofList [ (13, Single.NaN) ; (25, 0.25f) ]
  |> Bare.encode_map (Bare.success Bare.encode_i32) Bare.encode_f32
  |> expect_to_fail_with "value is not a number"

[<Test>]
let Type_map () =
  Map.ofList [ (13s, "Hello World") ; (14s, "How are you?") ; (25s, "This is cool!") ]
  |> failable_roundtrip
    (Bare.encode_map (Bare.success Bare.encode_i16) (Bare.success Bare.encode_string))
    (Bare.decode_map Bare.decode_i16 Bare.decode_string)

type MyUnion =
  | First
  | Second of int
  | Fourth of bool * string * byte array

let private myUnion_encoder value =
  match value with
  | First -> Bare.void_case 1u
  | Second payload -> Bare.union_case 2u (Bare.encode_i32 payload)
  | Fourth (a, b, c) ->
      (Bare.success Bare.encode_bool) a
      |> Bare.andThen (Bare.success Bare.encode_string) b
      |> Bare.andThen (Bare.encode_data_fixed_length 2) c
      |> Bare.failable_case 4u

let myUnion_decoder identifier =
  match identifier with
  | 1u ->
      Bare.from_value First
  | 2u ->
      Bare.decode_complex Second
      |> Bare.apply Bare.decode_i32
  | 4u ->
      Bare.decode_complex (fun a b c -> Fourth (a, b, c))
      |> Bare.apply Bare.decode_bool
      |> Bare.apply Bare.decode_string
      |> Bare.apply (Bare.decode_data_fixed_length 2)
  | identifier ->
      Bare.error (sprintf "missing decoder for identifier %i" identifier)

[<Test>]
let Type_union_encodes_type_identifier_followed_by_the_encoded_value () =
  Second 255
  |> Bare.encode_union myUnion_encoder
  |> expect_to_succeed_with [| 0x02uy ; 0xFFuy ; 0x00uy ; 0x00uy ; 0x00uy |]
  First
  |> Bare.encode_union myUnion_encoder
  |> expect_to_succeed_with [| 0x01uy |]

[<Test>]
let Type_union_decodes_value_by_using_decoder_lookup () =
  [| 0x02uy ; 0xFFuy ; 0x00uy ; 0x00uy ; 0x00uy |]
  |> Bare.decode_union myUnion_decoder
  |> expect_decoded (Second 255)
  [| 0x01uy |]
  |> Bare.decode_union myUnion_decoder
  |> expect_decoded First

[<Test>]
let Type_union_returns_decoder_error () =
  [| 0x00uy |]
  |> Bare.decode_union myUnion_decoder
  |> expect_to_fail_with "missing decoder for identifier 0"
  [| 0x04uy ; 0x02uy ; 0x00uy ; 0x3Auy ; 0x4Euy |]
  |> Bare.decode_union myUnion_decoder
  |> expect_to_fail_with "data does not represent a boolean"

[<Test>]
let Type_union_only_takes_needed_bytes () =
  [| 0x01uy ; 0x3Cuy ; 0xBAuy ; 0x1Duy |]
  |> Bare.decode_union myUnion_decoder
  |> has_remaining_bytes [| 0x3Cuy ; 0xBAuy ; 0x1Duy |]

[<Test>]
let Type_union () =
  [ First
    Second 13
    Second 9913
    Fourth (true, "Hello", [| 0uy ; 1uy |])
    First
    Fourth (false, "World", [| 13uy ; 34uy |])
  ]
  |> List.iter (failable_roundtrip (Bare.encode_union myUnion_encoder) (Bare.decode_union myUnion_decoder))

type TestStruct =
  { A : int
    B : string
  }

let testStruct_encoder (value : TestStruct) =
  (Bare.success Bare.encode_i32) value.A
  |> Bare.andThen (Bare.success Bare.encode_string) value.B

let testStruct_decoder =
  Bare.decode_complex (fun a b -> { A = a ; B = b })
  |> Bare.apply Bare.decode_i32
  |> Bare.apply Bare.decode_string

[<Test>]
let Type_struct_concats_all_encoded_data () =
  { A = 255 ; B = "Hello" }
  |> testStruct_encoder
  |> expect_to_succeed_with [| 0xFFuy ; 0x00uy ; 0x00uy ; 0x00uy ; 0x05uy ; 0x48uy ; 0x65uy ; 0x6Cuy ; 0x6Cuy ; 0x6Fuy |]

[<Test>]
let Type_struct_only_takes_needed_bytes () =
  [| 0xFFuy ; 0x00uy ; 0x00uy ; 0x00uy ; 0x00uy ; 0x3Auy ; 0xF3uy |]
  |> testStruct_decoder
  |> has_remaining_bytes [| 0x3Auy ; 0xF3uy |]

[<Test>]
let Type_struct_returns_error () =
  [| 0xFFuy ; 0x00uy ; 0x00uy ; 0x3Auy ; 0xF3uy |]
  |> testStruct_decoder
  |> expect_to_fail_with "wrong data for uint"

[<Test>]
let Type_struct () =
  [ { A = 13 ; B = "Hello" }
    { A = -51 ; B = "" }
    { A = 255 ; B = "Cool!" }
    { A = 0 ; B = "What a wonderful thing" }
  ]
  |> List.iter (failable_roundtrip testStruct_encoder testStruct_decoder)