using System;
using System.Text;
using System.Linq;

namespace Bare.CodeGen
{
    internal static class Utils
    {
        internal const string BareClass = "BareNET.Bare";
        internal const string Default_generated_encoding_class = "Encoding";

        internal static void Generate_header(DateTime generationTime, StringBuilder builder)
        {
            builder.AppendLine("//////////////////////////////////////////////////");
            builder.AppendLine($"// Generated code by BareNET - {generationTime:g} //");
            builder.AppendLine("//////////////////////////////////////////////////");
        }

        internal static string As_uppercase_name(string name) => $"{name.Substring(0, 1).ToUpper()}{name.Substring(1)}";
        internal static string As_lowercase_name(string name) => $"{name.Substring(0, 1).ToLower()}{name.Substring(1)}";

        internal static string With_indentation(this string code, int level, int? spaces)
        {
            var builder = new StringBuilder();
            var lines = code.Split(Environment.NewLine).ToArray();
            var tab = spaces.HasValue ? new String(' ', level * spaces.Value) : new string('\t', level);
            if (lines.Length == 1)
            {
                builder.AppendFormat("{0}{1}", tab, lines.First());
            }
            else
            {
                for (var i = 0; i < lines.Length; i++)
                {
                    builder.AppendFormat("{0}{1}", tab, lines[i]);
                    if (i < lines.Length-1) builder.AppendLine();
                }
            }
            return builder.ToString();
        }
    }
}