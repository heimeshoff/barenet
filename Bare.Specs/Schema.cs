﻿using System.IO;
using System.Linq;
using System.Text;
using BareNET.Schema;
using NUnit.Framework;

namespace Bare.Specs
{
    [TestFixture]
    public class Schema
    {
        [Test]
        public void Parse_schema_with_trailing_newline()
        {
            const string schema = @"
type PublicKey data<128>

";

            var ast = Parse(schema);
            Assert.AreEqual(
                new UserType[]
                {
                    new NamedUserType("PublicKey", new PrimitiveType(TypeKind.Data, 128))
                },
                ast);
        }

        [Test]
        public void Parse_schema_fails_when_typename_begins_with_a_lowercase_letter()
        {
            var schema = "enum namedEnum { FIRST }";
            Assert.Throws<System.FormatException>(() => Parse(schema));
            schema = "type namedType i8";
            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_fails_when_typename_begins_with_a_digit()
        {
            var schema = "enum 2NamedEnum { FIRST }";
            Assert.Throws<System.FormatException>(() => Parse(schema));
            schema = "type 2NamedType i8";
            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_fails_when_typename_contains_special_characters()
        {
            var schema = "enum Für { FIRST }";
            Assert.Throws<System.FormatException>(() => Parse(schema));
            schema = "type Überfähre i8";
            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_fails_when_typename_contains_underscore()
        {
            var schema = "enum Free_doom { FIRST }";
            Assert.Throws<System.FormatException>(() => Parse(schema));
            schema = "type Back_to_you i8";
            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_typename_allows_digits()
        {
            var schema = "enum NamedEnum2 { FIRST }";
            Assert.AreEqual(
                new UserType[]
                {
                    new EnumUserType("NamedEnum2", new EnumType(new [] { new EnumValue("FIRST", null) }))
                },
                Parse(schema));
            schema = "type Number64 i64";
            Assert.AreEqual(
                new UserType[]
                {
                    new NamedUserType("Number64", new PrimitiveType(TypeKind.I64, null))
                },
                Parse(schema));
        }

        [Test]
        public void Parse_schema_fails_when_struct_field_contains_special_characters()
        {
            const string schema = "type NamedStruct { Täöüß: i32 }";

            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_fails_when_struct_field_contains_a_non_letter()
        {
            const string schema = "type NamedStruct { Number4: i32 }";

            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_fails_when_struct_field_contains_an_underscore()
        {
            const string schema = "type NamedStruct { Number_One: i32 }";

            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_fails_when_enum_value_is_not_full_uppercase()
        {
            const string schema = "enum NamedEnum { FirstValue }";

            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_fails_when_enum_value_begins_with_a_digit()
        {
            const string schema = "enum NamedEnum { 1ST }";

            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_fails_when_enum_value_begins_with_an_underscore()
        {
            const string schema = "enum NamedEnum { _FIRST }";

            Assert.Throws<System.FormatException>(() => Parse(schema));
        }

        [Test]
        public void Parse_schema_enum_value_allows_digits()
        {
            const string schema = "enum NamedEnum { FRIENDS4EVER }";

            var ast = Parse(schema);
            Assert.AreEqual(
                new UserType[]
                {
                    new EnumUserType("NamedEnum", new EnumType(new [] { new EnumValue("FRIENDS4EVER", null) }))
                },
                ast);
        }

        [Test]
        public void Parse_schema_enum_value_allows_underscores()
        {
            const string schema = "enum NamedEnum { FIRST_VALUE }";

            var ast = Parse(schema);
            Assert.AreEqual(
                new UserType[]
                {
                    new EnumUserType("NamedEnum", new EnumType(new [] { new EnumValue("FIRST_VALUE", null) }))
                },
                ast);
        }

        [Test]
        public void Parse_schema()
        {
            const string schema = @"
type PublicKey data<128>
type Time string # ISO 8601

enum Department {
 ACCOUNTING
 ADMINISTRATION
 CUSTOMER_SERVICE
 DEVELOPMENT

 # Reserved for the CEO
 JSMITH = 99
}

type Customer {
 name: string
 email: string
 address: Address
 orders: []{
   orderId: i64
   quantity: i32
 }
 metadata: map[string]data
}

type Employee {
 name: string
 email: string
 address: Address
 department: Department
 hireDate: Time
 publicKey: optional<PublicKey>
 metadata: map[string]data
}

type TerminatedEmployee void

type Person (Customer = 1 | Employee | TerminatedEmployee)

type Address {
 address: [4]string
 city: string
 state: string
 country: string
}";

            var ast = Parse(schema);

            Assert.AreEqual(
                new UserType[]
                {
                    new NamedUserType("PublicKey", new PrimitiveType(TypeKind.Data, 128)),
                    new NamedUserType("Time", new PrimitiveType(TypeKind.String, null)),
                    new EnumUserType("Department", new EnumType(new []
                    {
                        new EnumValue("ACCOUNTING", null),
                        new EnumValue("ADMINISTRATION", null),
                        new EnumValue("CUSTOMER_SERVICE", null),
                        new EnumValue("DEVELOPMENT", null),
                        new EnumValue("JSMITH", 99u),
                    })),
                    new NamedUserType("Customer", new StructType(new []
                    {
                        new StructField("name", new PrimitiveType(TypeKind.String, null)),
                        new StructField("email", new PrimitiveType(TypeKind.String, null)),
                        new StructField("address", new UserTypeName("Address")),
                        new StructField("orders", new ListType(new StructType(new []
                        {
                            new StructField("orderId", new PrimitiveType(TypeKind.I64, null)),
                            new StructField("quantity", new PrimitiveType(TypeKind.I32, null)),
                        }), null)),
                        new StructField("metadata", new MapType(
                            new PrimitiveType(TypeKind.String, null),
                            new PrimitiveType(TypeKind.Data, null)
                        ))
                    })),
                    new NamedUserType("Employee", new StructType(new []
                    {
                        new StructField("name", new PrimitiveType(TypeKind.String, null)),
                        new StructField("email", new PrimitiveType(TypeKind.String, null)),
                        new StructField("address", new UserTypeName("Address")),
                        new StructField("department", new UserTypeName("Department")),
                        new StructField("hireDate", new UserTypeName("Time")),
                        new StructField("publicKey", new OptionalType(new UserTypeName("PublicKey"))),
                        new StructField("metadata", new MapType(
                            new PrimitiveType(TypeKind.String, null),
                            new PrimitiveType(TypeKind.Data, null)
                        ))
                    })),
                    new NamedUserType("TerminatedEmployee", new PrimitiveType(TypeKind.Void, null)),
                    new NamedUserType("Person", new UnionType(new []
                    {
                        new UnionMember(new UserTypeName("Customer"), 1),
                        new UnionMember(new UserTypeName("Employee"), null),
                        new UnionMember(new UserTypeName("TerminatedEmployee"), null)
                    })),
                    new NamedUserType("Address", new StructType(new []
                    {
                        new StructField("address", new ListType(new PrimitiveType(TypeKind.String, null), 4)),
                        new StructField("city", new PrimitiveType(TypeKind.String, null)),
                        new StructField("state", new PrimitiveType(TypeKind.String, null)),
                        new StructField("country", new PrimitiveType(TypeKind.String, null))
                    }))
                },
                ast);
        }

        private static UserType[] Parse(string schema)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(schema)))
            using (var scanner = new Scanner(new StreamReader(stream, Encoding.UTF8)))
                return Parser.Parse(scanner).ToArray();
        }
    }
}