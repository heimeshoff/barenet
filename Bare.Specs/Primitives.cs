﻿using System;
using NUnit.Framework;

namespace Bare.Specs
{
    internal enum TestEnum
    {
        First = 2,
        Second,
        Third = 5
    }

    internal enum FailingEnum
    {
        NegativeValue = -20
    }

    [TestFixture]
    public class Primitives
    {
        [Test]
        public void Type_u8_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_u8(new byte[] { 0x01, 0x02, 0x03 });
            Assert.AreEqual(new byte[] { 0x02, 0x03 }, rest);
        }

        [Test]
        public void Type_u8()
        {
            for(var value = byte.MinValue; value < byte.MaxValue; value++)
            {
                var data = BareNET.Bare.Encode_u8(value);
                var (decoded, _) = BareNET.Bare.Decode_u8(data);
                Assert.AreEqual(value, decoded);
            }
        }

        [Test]
        public void Type_u16_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_u16(new byte[] { 0x01, 0x02, 0x03 });
            Assert.AreEqual(new byte[] { 0x03 }, rest);
        }

        [Test]
        public void Type_u16_is_encoded_in_little_endian()
        {
            var data = BareNET.Bare.Encode_u16(0x0102);
            Assert.AreEqual(new byte[] { 0x02, 0x01 }, data);
        }

        [Test]
        public void Type_u16()
        {
            for (var value = ushort.MinValue; value < ushort.MaxValue; value++)
            {
                var data = BareNET.Bare.Encode_u16(value);
                var (decoded, _) = BareNET.Bare.Decode_u16(data);
                Assert.AreEqual(value, decoded);
            }
        }

        [Test]
        public void Type_u32_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_u32(new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 });
            Assert.AreEqual(new byte[] { 0x05, 0x06 }, rest);
        }

        [Test]
        public void Type_u32_is_encoded_in_little_endian()
        {
            var data = BareNET.Bare.Encode_u32(0x01020304);
            Assert.AreEqual(new byte[] { 0x04, 0x03, 0x02, 0x01 }, data);
        }

        [Test]
        public void Type_u32()
        {
            for (var value = uint.MaxValue;; value>>=8)
            {
                var data = BareNET.Bare.Encode_u32(value);
                var (decoded, _) = BareNET.Bare.Decode_u32(data);
                Assert.AreEqual(value, decoded);
                if (value == 0x00) break;
            }
        }

        [Test]
        public void Type_u64_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_u64(new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10 });
            Assert.AreEqual(new byte[] { 0x09, 0x10 }, rest);
        }

        [Test]
        public void Type_u64_is_encoded_in_little_endian()
        {
            var data = BareNET.Bare.Encode_u64(0x0102030405060708);
            Assert.AreEqual(new byte[] { 0x08, 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01 }, data);
        }

        [Test]
        public void Type_u64()
        {
            for (var value = ulong.MaxValue;; value>>=8)
            {
                var data = BareNET.Bare.Encode_u64(value);
                var (decoded, _) = BareNET.Bare.Decode_u64(data);
                Assert.AreEqual(value, decoded);
                if (value == 0x00) break;
            }
        }

        [Test]
        public void Type_uint_encodes_in_variable_length__value_is_splitted_in_7_bit_groups__most_significant_bit_is_not_set_for_last_octet()
        {
            Assert.AreEqual(new byte[] { 0b0111_1111 }, BareNET.Bare.Encode_uint(127));
            Assert.AreEqual(new byte[] { 0b1000_0000, 0b0000_0001 }, BareNET.Bare.Encode_uint(128));
            Assert.AreEqual(new byte[] { 0b1000_0000, 0b1000_0000, 0b0000_0001 }, BareNET.Bare.Encode_uint(16384));
            Assert.AreEqual(new byte[] { 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b0111_1111 }, BareNET.Bare.Encode_uint(72057594037927935));
            Assert.AreEqual(new byte[] { 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b0000_0001 }, BareNET.Bare.Encode_uint(ulong.MaxValue));
        }

        [Test]
        public void Type_uint_is_encoded_in_little_endian()
        {
            var data = BareNET.Bare.Encode_uint(0b1110011_0011100_0001111);
            Assert.AreEqual(new byte[] { 0b10001111, 0b10011100, 0b01110011 }, data);
        }

        [Test]
        public void Type_uint_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_uint(new byte[] { 0b10001111, 0b01111000, 0x02, 0x3E });
            Assert.AreEqual(new byte[] { 0x02, 0x3E }, rest);
        }

        [Test]
        public void Type_uint()
        {
            for (var value = ulong.MaxValue; ; value >>= 8)
            {
                var data = BareNET.Bare.Encode_uint(value);
                var (decoded, _) = BareNET.Bare.Decode_uint(data);
                Assert.AreEqual(value, decoded);
                if (value == 0x00) break;
            }
        }

        [Test]
        public void Type_i8_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_i8(new byte[] { 0x01, 0x02, 0x03 });
            Assert.AreEqual(new byte[] { 0x02, 0x03 }, rest);
        }

        [Test]
        public void Type_i8_uses_two_s_complement()
        {
            var data = BareNET.Bare.Encode_i8(-1);
            Assert.AreEqual(new byte[] { 0xFF }, data);
        }

        [Test]
        public void Type_i8()
        {
            for (var value = sbyte.MinValue; value < sbyte.MaxValue; value++)
            {
                var data = BareNET.Bare.Encode_i8(value);
                var (decoded, _) = BareNET.Bare.Decode_i8(data);
                Assert.AreEqual(value, decoded);
            }
        }

        [Test]
        public void Type_i16_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_i16(new byte[] { 0x01, 0x02, 0x03 });
            Assert.AreEqual(new byte[] { 0x03 }, rest);
        }

        [Test]
        public void Type_i16_uses_two_s_complement()
        {
            var data = BareNET.Bare.Encode_i16(-1);
            Assert.AreEqual(new byte[] { 0xFF, 0xFF }, data);
        }

        [Test]
        public void Type_i16_is_encoded_in_little_endian()
        {
            var data = BareNET.Bare.Encode_i16(0x0102);
            Assert.AreEqual(new byte[] { 0x02, 0x01 }, data);
        }

        [Test]
        public void Type_i16()
        {
            for (var value = short.MinValue; value < short.MaxValue; value++)
            {
                var data = BareNET.Bare.Encode_i16(value);
                var (decoded, _) = BareNET.Bare.Decode_i16(data);
                Assert.AreEqual(value, decoded);
            }
        }

        [Test]
        public void Type_i32_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_i32(new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06 });
            Assert.AreEqual(new byte[] { 0x05, 0x06 }, rest);
        }

        [Test]
        public void Type_i32_uses_two_s_complement()
        {
            var data = BareNET.Bare.Encode_i32(-1);
            Assert.AreEqual(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF }, data);
        }

        [Test]
        public void Type_i32_is_encoded_in_little_endian()
        {
            var data = BareNET.Bare.Encode_i32(0x01020304);
            Assert.AreEqual(new byte[] { 0x04, 0x03, 0x02, 0x01 }, data);
        }

        [Test]
        public void Type_i32()
        {
            for (var value = int.MaxValue;; value>>=8)
            {
                var data = BareNET.Bare.Encode_i32(value);
                var (decoded, _) = BareNET.Bare.Decode_i32(data);
                Assert.AreEqual(value, decoded);
                if (value == 0x00) break;
            }

            for (var value = int.MinValue; ; value >>= 8)
            {
                var data = BareNET.Bare.Encode_i32(value);
                var (decoded, _) = BareNET.Bare.Decode_i32(data);
                Assert.AreEqual(value, decoded);
                if (value == -1) break;
            }
        }

        [Test]
        public void Type_i64_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_i64(new byte[] { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10 });
            Assert.AreEqual(new byte[] { 0x09, 0x10 }, rest);
        }

        [Test]
        public void Type_i64_uses_two_s_complement()
        {
            var data = BareNET.Bare.Encode_i64(-1);
            Assert.AreEqual(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }, data);
        }

        [Test]
        public void Type_i64_is_encoded_in_little_endian()
        {
            var data = BareNET.Bare.Encode_i64(0x0102030405060708);
            Assert.AreEqual(new byte[] { 0x08, 0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01 }, data);
        }

        [Test]
        public void Type_i64()
        {
            for (var value = long.MaxValue;; value>>=8)
            {
                var data = BareNET.Bare.Encode_i64(value);
                var (decoded, _) = BareNET.Bare.Decode_i64(data);
                Assert.AreEqual(value, decoded);
                if (value == 0x00) break;
            }

            for (var value = long.MinValue; ; value >>= 8)
            {
                var data = BareNET.Bare.Encode_i64(value);
                var (decoded, _) = BareNET.Bare.Decode_i64(data);
                Assert.AreEqual(value, decoded);
                if (value == -1) break;
            }
        }

        [Test]
        public void Type_int_encodes_in_variable_length__value_is_splitted_in_7_bit_groups__most_significant_bit_is_not_set_for_last_octet()
        {
            Assert.AreEqual(new byte[] { 0b0111_1110 }, BareNET.Bare.Encode_int(63));
            Assert.AreEqual(new byte[] { 0b1000_0000, 0b0000_0001 }, BareNET.Bare.Encode_int(64));
            Assert.AreEqual(new byte[] { 0b1000_0000, 0b1000_0000, 0b0000_0001 }, BareNET.Bare.Encode_int(8192));
            Assert.AreEqual(new byte[] { 0b1111_1110, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b1111_1111, 0b0000_0001 }, BareNET.Bare.Encode_int(long.MaxValue));
        }

        [Test]
        public void Type_int_is_encoded_in_zig_zag()
        {
            Assert.AreEqual(new byte[] { 0b00000011 }, BareNET.Bare.Encode_int(-2));
            Assert.AreEqual(new byte[] { 0b00000001 }, BareNET.Bare.Encode_int(-1));
            Assert.AreEqual(new byte[] { 0b00000000 }, BareNET.Bare.Encode_int(0));
            Assert.AreEqual(new byte[] { 0b00000010 }, BareNET.Bare.Encode_int(1));
            Assert.AreEqual(new byte[] { 0b00000100 }, BareNET.Bare.Encode_int(2));
        }

        [Test]
        public void Type_int_is_encoded_in_little_endian()
        {
            var data = BareNET.Bare.Encode_int(0b1110011_0011100_0001111);
            Assert.AreEqual(new byte[] { 0b10011110, 0b10111000, 0b11100110, 0b00000001 }, data);
        }

        [Test]
        public void Type_int_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_int(new byte[] { 0b10001111, 0b01111000, 0x02, 0x3E });
            Assert.AreEqual(new byte[] { 0x02, 0x3E }, rest);
        }

        [Test]
        public void Type_int()
        {
            for (var value = long.MaxValue; ; value >>= 8)
            {
                var data = BareNET.Bare.Encode_int(value);
                var (decoded, _) = BareNET.Bare.Decode_int(data);
                Assert.AreEqual(value, decoded);
                if (value == 0x00) break;
            }

            for (var value = long.MinValue; ; value >>= 8)
            {
                var data = BareNET.Bare.Encode_int(value);
                var (decoded, _) = BareNET.Bare.Decode_int(data);
                Assert.AreEqual(value, decoded);
                if (value == -1) break;
            }
        }

        [Test]
        public void Type_f32_raises_exception_if_value_is_NaN()
        {
            Assert.Throws<ArgumentException>(() => BareNET.Bare.Encode_f32(float.NaN));
        }

        [Test]
        public void Type_f32_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_f32(new byte[] { 0x00, 0x1E, 0x22, 0x3F, 0x8A, 0xE4 });
            Assert.AreEqual(new byte[] { 0x8A, 0xE4 }, rest);
        }

        [Test]
        public void Type_f32()
        {
            var r = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            for (var i = 0; i < 20; i++)
            {
                var value = (float)r.NextDouble();
                var data = BareNET.Bare.Encode_f32(value);
                var (decoded, _) = BareNET.Bare.Decode_f32(data);
                Assert.AreEqual(value, decoded);
            }
        }

        [Test]
        public void Type_f64_raises_exception_if_value_is_NaN()
        {
            Assert.Throws<ArgumentException>(() => BareNET.Bare.Encode_f64(double.NaN));
        }

        [Test]
        public void Type_f64_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_f64(new byte[] { 0x00, 0x1E, 0x22, 0x3F, 0x00, 0x1E, 0x22, 0x3F, 0x8A, 0xE4 });
            Assert.AreEqual(new byte[] { 0x8A, 0xE4 }, rest);
        }

        [Test]
        public void Type_f64()
        {
            var r = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            for (var i = 0; i < 20; i++)
            {
                var value = r.NextDouble();
                var data = BareNET.Bare.Encode_f64(value);
                var (decoded, _) = BareNET.Bare.Decode_f64(data);
                Assert.AreEqual(value, decoded);
            }
        }

        [Test]
        public void Type_bool_greater_one_throws_exception()
        {
            Assert.Throws<ArgumentException>(() => { BareNET.Bare.Decode_bool(new byte[] {0x02}); });
        }

        [Test]
        public void Type_bool()
        {
            var (bTrue, _) = BareNET.Bare.Decode_bool(BareNET.Bare.Encode_bool(true));
            Assert.AreEqual(true, bTrue);
            var (bFalse, _) = BareNET.Bare.Decode_bool(BareNET.Bare.Encode_bool(false));
            Assert.AreEqual(false, bFalse);
        }

        [Test]
        public void Type_enum_encoding_value_not_a_member_of_enum_throws_exception()
        {
            Assert.Throws<ArgumentException>(() => { BareNET.Bare.Decode_enum<TestEnum>(new byte[] { 0x01 }); });
        }

        [Test]
        public void Type_enum_decoding_negative_value_throws_exception()
        {
            Assert.Throws<ArgumentException>(() => { BareNET.Bare.Encode_enum(FailingEnum.NegativeValue); });
        }

        [Test]
        public void Type_enum()
        {
            var values = new[] {TestEnum.First, TestEnum.Second, TestEnum.Third};
            foreach (var value in values)
            {
                var data = BareNET.Bare.Encode_enum(value);
                var (decoded, _) = BareNET.Bare.Decode_enum<TestEnum>(data);
                Assert.AreEqual(value, decoded);
            }
        }

        [Test]
        public void Type_string_encodes_length_in_octets_as_uint_in_first_place()
        {
            var data = BareNET.Bare.Encode_string("Hello World");
            Assert.AreEqual(new byte[] { 0x0B, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64 }, data);
        }

        [Test]
        public void Type_string_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_string(new byte[] { 0x0B, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64, 0x02, 0xF2 });
            Assert.AreEqual(new byte[] { 0x02, 0xF2 }, rest);
        }

        [Test]
        public void Type_string()
        {
            var values = new[]
            {
                "Hello World!",
                "∮ E⋅da = Q,  n → ∞, ∑ f(i) = ∏ g(i), ∀x∈ℝ: ⌈x⌉ = −⌊−x⌋, α ∧ ¬β = ¬(¬α ∨ β)",
                @"Οὐχὶ ταὐτὰ παρίσταταί μοι γιγνώσκειν, ὦ ἄνδρες ᾿Αθηναῖοι,
                ὅταν τ᾿ εἰς τὰ πράγματα ἀποβλέψω καὶ ὅταν πρὸς τοὺς
                λόγους οὓς ἀκούω· τοὺς μὲν γὰρ λόγους περὶ τοῦ
                τιμωρήσασθαι Φίλιππον ὁρῶ γιγνομένους, τὰ δὲ πράγματ᾿
                εἰς τοῦτο προήκοντα,  ὥσθ᾿ ὅπως μὴ πεισόμεθ᾿ αὐτοὶ
                πρότερον κακῶς σκέψασθαι δέον. οὐδέν οὖν ἄλλο μοι δοκοῦσιν
                οἱ τὰ τοιαῦτα λέγοντες ἢ τὴν ὑπόθεσιν, περὶ ἧς βουλεύεσθαι,
                οὐχὶ τὴν οὖσαν παριστάντες ὑμῖν ἁμαρτάνειν. ἐγὼ δέ, ὅτι μέν
                ποτ᾿ ἐξῆν τῇ πόλει καὶ τὰ αὑτῆς ἔχειν ἀσφαλῶς καὶ Φίλιππον
                τιμωρήσασθαι, καὶ μάλ᾿ ἀκριβῶς οἶδα· ἐπ᾿ ἐμοῦ γάρ, οὐ πάλαι
                γέγονεν ταῦτ᾿ ἀμφότερα· νῦν μέντοι πέπεισμαι τοῦθ᾿ ἱκανὸν
                προλαβεῖν ἡμῖν εἶναι τὴν πρώτην, ὅπως τοὺς συμμάχους
                σώσομεν. ἐὰν γὰρ τοῦτο βεβαίως ὑπάρξῃ, τότε καὶ περὶ τοῦ
                τίνα τιμωρήσεταί τις καὶ ὃν τρόπον ἐξέσται σκοπεῖν· πρὶν δὲ
                τὴν ἀρχὴν ὀρθῶς ὑποθέσθαι, μάταιον ἡγοῦμαι περὶ τῆς
                τελευτῆς ὁντινοῦν ποιεῖσθαι λόγον."
            };
            foreach (var value in values)
            {
                var data = BareNET.Bare.Encode_string(value);
                var (decoded, _) = BareNET.Bare.Decode_string(data);
                Assert.AreEqual(value, decoded);
            }
        }

        [Test]
        public void Type_data_fixed_length_encodes_data_literally_in_the_message()
        {
            var data = BareNET.Bare.Encode_data_fixed_length(4, new byte[] { 0x01, 0x02, 0x03, 0x04 });
            Assert.AreEqual(new byte[] { 0x01, 0x02, 0x03, 0x04 }, data);
        }

        [Test]
        public void Type_data_fixed_length_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_data_fixed_length(4, new byte[] { 0x01, 0x02, 0x03, 0x04, 0xF2, 0xAC });
            Assert.AreEqual(new byte[] { 0xF2, 0xAC }, rest);
        }

        [Test]
        public void Type_data_fixed_length__given_data_is_less_than_given_length_throws_exception()
        {
            Assert.Throws<ArgumentException>(() => { BareNET.Bare.Encode_data_fixed_length(5, new byte[] { 0x02, 0x03 }); });
        }

        [Test]
        public void Type_data_fixed_length__zero_length_throws_exception()
        {
            Assert.Throws<ArgumentException>(() => { BareNET.Bare.Encode_data_fixed_length(0, new byte[0]); });
        }

        [Test]
        public void Type_data_fixed_length()
        {
            var values = new []
            {
                new byte[] { 0x01, 0x02, 0x03, 0x04 },
                new byte[] { 0x05, 0x06, 0x07, 0x08 },
                new byte[] { 0x09, 0x0A, 0x0B, 0x0C },
                new byte[] { 0x0D, 0x0E, 0x0F, 0x10 }
            };
            foreach (var value in values)
            {
                var data = BareNET.Bare.Encode_data_fixed_length(4, value);
                var (decoded, _) = BareNET.Bare.Decode_data_fixed_length(4, data);
                Assert.AreEqual(value, decoded);
            }
        }

        [Test]
        public void Type_data_encodes_length_at_first_place_followed_by_the_data()
        {
            var data = BareNET.Bare.Encode_data(new byte[] { 0x02, 0x05, 0xA2 });
            Assert.AreEqual(new byte[] { 0x03, 0x02, 0x05, 0xA2 }, data);
        }

        [Test]
        public void Type_data_takes_only_needed_bytes()
        {
            var (_, rest) = BareNET.Bare.Decode_data(new byte[] { 0x04, 0x01, 0x02, 0x03, 0x04, 0xF2, 0xAC });
            Assert.AreEqual(new byte[] { 0xF2, 0xAC }, rest);
        }

        [Test]
        public void Type_data_empty_data()
        {
            Assert.AreEqual(new byte[] { 0x00 }, BareNET.Bare.Encode_data(new byte[0]));
        }

        [Test]
        public void Type_data()
        {
            var values = new[]
            {
                new byte[] { 0x01, 0x02, 0x03, 0x04 },
                new byte[] { 0x05, 0x06 },
                new byte[] { 0x09, 0x0A, 0x0B, 0x0C, 0xFE, 0xAA, 0xEE, 0x2F },
                new byte[] { 0x0D }
            };
            foreach (var value in values)
            {
                var data = BareNET.Bare.Encode_data(value);
                var (decoded, _) = BareNET.Bare.Decode_data(data);
                Assert.AreEqual(value, decoded);
            }
        }
    }
}